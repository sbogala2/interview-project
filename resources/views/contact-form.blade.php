<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Contact Form - Interview Project</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	<div class="container">
		<br>
		<div class="card">
			<div class="card-header">
				<h3 id="responseMessage" class="text-center" style="color: #0cd50c;">
					
				</h3>
				<?php
				  $alertType = '';
				  $message   = '';
				  if(Session::has('error'))
				  {
				    $alertType = 'danger';
				    $message   = Session::get('error');
				  }
				  else if(Session::has('success'))
				  {
				    $alertType = 'success';
				    $message   = Session::get('success');
				  }
				?>

				@if($alertType && $message)
				  <div class="alert alert-{{$alertType}} alert-dismissible" role="alert">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">×</span>
				    </button> 
				    {{ $message }}
				  </div>
				@endif

				@if($errors->all())
				  <div class="bg-danger" style="padding:5px 10px; color: white;">
				    <strong>Following errors occured:</strong>
				    <ul>
				    @foreach($errors->all() as $error)
				      <li>{{ $error }}</li>
				    @endforeach
				    </ul>
				  </div>
				@endif

			</div>
			<div class="card-body">
				<div class="card-title text-center">
					<h1>Contact Form</h1>
				</div>
				<hr>
				<form action="{{ URL::current() }}" method="POST" id="contactForm">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="firstName">First Name</label>
							<input type="text" name="first_name" class="form-control" id="firstName" placeholder="First Name">
							<span class="field-error" for="first_name"></span>
						</div>
						<div class="form-group col-md-6">
							<label for="lastName">Last Name</label>
							<input type="text" name="last_name" class="form-control" id="lastName" placeholder="Last Name">
							<span class="field-error" for="last_name"></span>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="userEmail">Email</label>
							<input type="text" name="email" class="form-control" id="userEmail" placeholder="some@mail.com">
							<span class="field-error" for="email"></span>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="street">Street</label>
							<input type="text" name="street" class="form-control" id="street" placeholder="Street">
							<span class="field-error" for="street"></span>
						</div>
						<div class="form-group col-md-6">
							<label for="city">City</label>
							<input type="text" name="city" class="form-control" id="city" placeholder="City">
							<span class="field-error" for="city"></span>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="state">State</label>
							<input type="text" name="state" class="form-control" id="state" placeholder="State">
							<span class="field-error" for="state"></span>
						</div>
						<div class="form-group col-md-6">
							<label for="zip">Zip</label>
							<input type="number" name="zip" class="form-control" id="zip" placeholder="Zip">
							<span class="field-error" for="zip"></span>
						</div>
					</div>
					{{ csrf_field() }}
					<div class="col-lg-12">
						<button id="formSubmit" type="submit" class="btn btn-dark btn-lg float-right">Submit</button>
					</div>
				</form>	
			</div>
		</div>
	</div>
</body>
<style type="text/css">
	.field-error {
		font-weight: bold;
	    color: red;
	    margin-left: 7px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('#contactForm').submit(function(e){
			e.preventDefault();
			$contactForm = $(this);
			$formInput = $contactForm.find(':input');
			$.ajax({
	            type: 'POST',
	            url : $contactForm.attr('action'),
	            data: new FormData($contactForm[0]),
	            processData : false,
				contentType : false,
	            beforeSend : function() {
	            	$('#responseMessage').hide();
	            	$('.field-error').hide();
					$formInput.addClass('disabled').prop('disabled', 1);
				},
	            success:function(response) {
	             	if(response.status == 'error') {
	             		if(response.errors) {
	             			$.each(response.errors, function(name, messages){
	             				$('span[for="'+name+'"]').text((messages[0]? messages[0]: 'Error')).show();
	             			});
	             		}
	             	} else if(response.status == 'success') {
	             		$('#responseMessage').text(((response.message)? response.message: 'Success')).show();
	             		$contactForm[0].reset();
	             	}
	            },
	            error: function(response) {
	            	
	            },
	            complete : function() {
	            	$formInput.removeClass('disabled').prop('disabled', 0);
	            }
	        });
		});
	});
</script>
</html>