<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Request;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name'  => 'sometimes|max:255',
            'email'      => 'required|email|max:255',
            'street'     => 'sometimes|max:255',
            'city'       => 'sometimes|max:255',
            'state'      => 'sometimes|max:255',
            'zip'        => 'sometimes|max:10'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if(Request::ajax()) {
            $response = [
                            'status'    => 'error',
                            'message'   => 'Following fields errors occured',
                            'errors'    => $validator->errors()
                        ];
            throw new HttpResponseException(response()->json($response, 200));
        } else {
            parent::failedValidation($validator);
        }
    }
}
