<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Contact;
use CEE\Salesforce\SyncObject;
 
class ContactController extends Controller
{
    public function index()
    {
        return view('contact-form');
    }

    public function submit(ContactRequest $request)
    {
        $status  = 'success';
        $message = 'Contact information saved successfully!';

        try
        {
            $contact                = new Contact();
            $contact->first_name    = $request->input('first_name');
            $contact->last_name     = $request->input('last_name');
            $contact->email         = $request->input('email');
            $contact->street        = $request->input('street');
            $contact->city          = $request->input('city');
            $contact->state         = $request->input('state');
            $contact->zip           = $request->input('zip');
            $contact->save();

            /**
             * Salesforce sync sample code
             */
            // $fields = [
            //             'FirstName' => $request->input('first_name'),
            //             'LastName'  => $request->input('last_name'),
            //             'Email'     => $request->input('email'),
            //             'Street'    => $request->input('street'),
            //             'City'      => $request->input('city'),
            //             'State'     => $request->input('state'),
            //             'Zip'       => $request->input('zip')
            //         ];
            // SyncObject::objectName('Contact')->id('00A10000001aBCde')->pushFields($fields)->push();
        }
        catch(Exception $e)
        {
            $status  = 'error';
            $message = $e->getMessage();
        }

        if($request->ajax())
        {
            return response()->json(['status' => $status, 'message' => $message]);
        }
        else
        {
            return redirect('/')->with($status, $message);
        }
    }
}